dset ^anl_surf125.%y4%m2
index ^anl_surf125.monthly.idx
undef 9.999E+20
title anl_surf125
*  produced by grib2ctl v0.9.12.5p41
dtype grib 255
options yrev template
ydef 145 linear -90.000000 1.25
xdef 288 linear 0.000000 1.250000
tdef 759 linear 00Z01Jan1958 1mo
zdef 1 linear 1 1
vars 9
DEPR2m  0 18,105,2 ** 2 m above ground Dew-point depression [K]
POTsfc  0 13,1,0  ** surface Potential temperature [K]
PRESsfc  0 1,1,0  ** surface Pressure [Pa]
PRMSLmsl  0 2,102,0 ** mean-sea level Pressure reduced to MSL [Pa]
RH2m  0 52,105,2 ** 2 m above ground Relative humidity [%]
SPFH2m  0 51,105,2 ** 2 m above ground Specific humidity [kg/kg]
TMP2m  0 11,105,2 ** 2 m above ground Temperature [K]
UGRD10m  0 33,105,10 ** 10 m above ground u-component of wind [m/s]
VGRD10m  0 34,105,10 ** 10 m above ground v-component of wind [m/s]
ENDVARS
