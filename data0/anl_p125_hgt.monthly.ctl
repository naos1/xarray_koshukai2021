dset ^anl_p125_hgt.%y4%m2
index ^anl_p125_hgt.monthly.idx
undef 9.999E+20
title anl_p125_hgt
*  produced by grib2ctl v0.9.12.5p41
dtype grib 255
options yrev template
ydef 145 linear -90.000000 1.25
xdef 288 linear 0.000000 1.250000
tdef 759 linear 00Z01Jan1958 1mo
*  z has 37 levels, for prs
zdef 37 levels
1000 975 950 925 900 875 850 825 800 775 750 700 650 600 550 500 450 400 350 300 250 225 200 175 150 125 100 70 50 30 20 10 7 5 3 2 1
vars 1
HGTprs 37 7,100,0 ** (profile) Geopotential height [gpm]
ENDVARS
