dset ^HGTprs.bin
undef 9.999E+20
title HGTprs
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef 39 linear 15Z27apr2021 3hr
zdef 16 levels 1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100
vars 1
HGTprs 16 0 **
ENDVARS
