dset ^ug.nc
dtype netcdf
undef 9.999e20 _FillValue
title GeostrophicUwind
options yrev
unpack scale_factor add_offset
ydef 145 linear -90 1.25
xdef 288 linear 0 1.25
tdef 1 linear 00Z01Jan1958 6hr
zdef 1 levels 500
*1000 975 950 925 900 875 850 825 800 775 750 700 650 600 550 500 450 400 350 300 250 225 200 175 150 125 100 70 50 30 20 10 7 5 3 2 1
vars 1
GeostrophicUwind=>ug 1 y,x *
ENDVARS
